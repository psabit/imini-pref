//
// Created by Alessandro on 2019-01-23.
//

#ifndef MINISAT_HEAPSTACK_H
#define MINISAT_HEAPSTACK_H

#include "minisat/mtl/Vec.h"
#include "minisat/mtl/Heap.h"
#include "minisat/mtl/Alg.h"
#include "minisat/mtl/IntMap.h"
#include "minisat/utils/Options.h"
#include "minisat/core/SolverTypes.h"

namespace Minisat {

    enum {
        pref_false,
        pref_true,
        no_pref
    };

    struct VarOrderLt {
        const IntMap<Var, double> &activity;

        bool operator()(Var x, Var y) const { return activity[x] > activity[y]; }

        VarOrderLt(const IntMap<Var, double> &act) : activity(act) {}
    };

    /*
    class VarPref {
        private:
            unsigned int pol: 1;
            unsigned int level: 31;

        public:
            bool hasPref(){ return level != 0; }
            Lit pref(Var var){ return mkLit}
    };
    */

    class PrefLevel {

        IntMap<Var, double>& heuristic;

        bool isTotal;

        Heap < Var, VarOrderLt>* heap;

    };


    class PreferenceManager {

    public:
        PreferenceManager(VMap<double> &_activity) : activity(_activity) {}

        ~PreferenceManager() {}

    private:

        //std::vector<Heap < Var, VarOrderLt>* > stack;
        std::vector<Level> stack;
        //std::vector<int> levels;
        VMap<double> &activity;
        //vec<VarPref> preference;
        IntMap<Var, int> levels;

    public:

        // TODO Modificare per passare total order o un heuristic personalizzata
        void addLevel(int level, bool total, IntMap<Var, VarOrderLt>& heuristic) {
            while (stack.size() <= level) {
                stack.push_back(new Heap<Var, VarOrderLt>(VarOrderLt(heuristic)));
            }
        }

        void setPreference(Lit lit, int level){

        }

        void push_back() { stack.push_back(new Heap<Var, VarOrderLt>(VarOrderLt(activity))); }

        void pop_back() {
            Heap <Var, VarOrderLt> *order_heap = stack[stack.size() - 1];
            stack.pop_back();
            delete order_heap;
        }

        Heap <Var, VarOrderLt> &back() { return *(stack[stack.size() - 1]); }

        bool empty() { return stack.empty(); }

        Heap <Var, VarOrderLt> &operator[](int level) { return *(stack[level]); }

        int size() { return stack.size(); }

        bool hasLevel(int level) { return stack.size() > level; }

        void Solver::setPreference(Var v, char p, int level){
            assert(p == pref_true || p == pref_false || p == no_pref);
            assert(decision[v]);

            if (preference.size() > v && level != preference[v].level && heap_stack.hasLevel(preference[v].level) && heap_stack[preference[v].level].inHeap(v)){       // Checks if the variable used to be in another level. Removes it in case. inHeap is required because the variable could have been removed by the solver.
                heap_stack[preference[v].level].remove(v);
            }

            //heap_stack.addLevel(level);

            VarPref var_pref;
            //var_pref.val    = p;
            //var_pref.level  = level;

            while (preference.size() <= v) preference.push(var_pref); // FIXME Check if it is correct. Should be, Var is an int.
            preference[v].val = p;
            preference[v].level = level;

            user_pol[v] = p == pref_true? l_True : p == pref_false ? l_False : l_Undef; // l_True sets variable 'v' to literal v. l_False sets variable 'v' to literal -v.

            insertVarOrder(v);
        }

    //TMP
    public:
        void print() {
            for (uint i = 0; i < stack.size(); ++i) {
                printf("Heap Level %d: ", i);
                (*stack[i]).print();
                printf("\n");
            }
        }
    };

}

#endif //MINISAT_HEAPSTACK_H
